from __future__ import annotations
from abc import ABC, abstractmethod

class Element:
    next: Element
    prev: Element
    value: int

    def __init__(self, value: int):
        self.value = value
        self.next = self.prev = None

    def __str__(self):
        return f"{self.value}"

    def __eq__(self, other) -> bool:
        return self.value == other.value

class ChainList(ABC):

    top: Element
    def __init__(self):
        self.top = None

    def __repr__(self):
        if self.top is None:
            return 'Liste vide'
        else:
            sortie = ''
            current = self.top
            while current is not None:
                sortie += str(current.value) + ' -> '
                current = current.next
            return sortie + 'None'

    @abstractmethod
    def insert(self, e):
        pass

    def delete(self, i: int):
        e = Element(i)
        if self.top == e:
            self.top = self.top.next
        else:
            prev = self.top
            current_nb = prev.next
            while current_nb is not None and not current_nb == e:
                prev = current_nb
                current_nb = prev.next
            if current_nb is None:
                Exception("Unknown number")
            else:
                prev.next = current_nb.next

    def get(self, i: int):
        base_nb = i
        current_nb = self.top
        i -= 1
        while not i == 0:
            current_nb = current_nb.next
            i -= 1
        print(f"La {base_nb}ème valeur est {current_nb}")






    @abstractmethod
    def is_empty(self) -> bool:
        return self.top is None

    def __len__(Chain):
        n = 0
        chainon = Chain
        while chainon is not None:
            n += 1
            chainon = chainon.next
        return n


class File(ChainList):
    def insert(self, e: Element):
        new_element = Element(e)

        if self.top is None:
            self.top = new_element
        else:
            current_nb = self.top
            while current_nb.next is not None:
                current_nb = current_nb.next
            current_nb.next = new_element

    def is_empty(self):
        pass
class OrderedList(ChainList):
    def insert(self, e: int):
        new_element = Element(e)

        if self.top is None:
            self.top = new_element
        elif self.top.value > new_element.value:
            temp = self.top
            self.top = new_element
            new_element.next = temp
        else:
            current_nb = self.top
            while current_nb.next is not None and current_nb.next.value < new_element.value:
                current_nb = current_nb.next
            if current_nb.next is not None and current_nb.next.value > new_element.value:
                temp = current_nb.next
                current_nb.next = new_element
                new_element.next = temp
            if current_nb.next is None:
                current_nb.next = new_element

    def is_empty(self):
        pass


f = File()
f.insert(2)
f.insert(4)
f.insert(1)
f.delete(2)
f.get(2)
print(repr(f))

o = OrderedList()
o.insert(3)
o.insert(4)
o.insert(8)
o.insert(1)
o.delete(1)
o.get(2)


print(repr(o))